document.addEventListener('DOMContentLoaded', function () {
    Highcharts.setOptions({
        colors: ['#00ff00', '#ffff00', '#ff0000'],
        chart: {
            backgroundColor: 'white',
            plotBorderColor: 'black'
        },
        title: {
            style: {
                color: 'black'
            }
        },
        subtitle: {
            style: {
                color: 'black'
            }
        },
        xAxis: {
            gridLineColor: '#707073',
            labels: {
                style: {
                    color: 'black'
                }
            },
            lineColor: '#707073',
            minorGridLineColor: '#505053',
            tickColor: '#707073',
            title: {
                style: {
                    color: '#A0A0A3',
                    text: "Time"
                }
            },
            type: 'datetime',
        },
        yAxis: {
            gridLineColor: '#707073',
            labels: {
                style: {
                    color: 'black'
                }
            },
            lineColor: '#707073',
            minorGridLineColor: '#505053',
            tickColor: '#707073',
            tickWidth: 1,
            title: {
                style: {
                    color: 'black'
                }
            }
        },
        tooltip: {
            backgroundColor: 'rgba(0, 0, 0, 0.85)',
            style: {
                color: '#F0F0F0'
            }
        },
        plotOptions: {
            series: {
                dataLabels: {
                    color: '#B0B0B3'
                },
                marker: {
                    lineColor: '#333'
                }
            },
            boxplot: {
                fillColor: '#505053'
            },
            candlestick: {
                lineColor: 'white'
            },
            errorbar: {
                color: 'white'
            }
        },
        legend: {
            itemStyle: {
                color: 'black'
            },
            itemHoverStyle: {
                color: '#BEBEBE'
            },
            itemHiddenStyle: {
                color: '#606063'
            }
        },
        credits: {
            style: {
                color: '#666'
            }
        },
        labels: {
            style: {
                color: '#707073'
            }
        },

        drilldown: {
            activeAxisLabelStyle: {
                color: '#F0F0F3'
            },
            activeDataLabelStyle: {
                color: '#F0F0F3'
            }
        },

        navigation: {
            buttonOptions: {
                symbolStroke: 'black',
                theme: {
                    fill: 'white'
                }
            }
        },

        // scroll charts
        rangeSelector: {
            buttonTheme: {
                fill: '#505053',
                stroke: '#000000',
                style: {
                    color: '#CCC'
                },
                states: {
                    hover: {
                        fill: '#707073',
                        stroke: '#000000',
                        style: {
                            color: 'white'
                        }
                    },
                    select: {
                        fill: '#000003',
                        stroke: '#000000',
                        style: {
                            color: 'white'
                        }
                    }
                }
            },
            inputBoxBorderColor: '#505053',
            inputStyle: {
                backgroundColor: '#333',
                color: 'silver'
            },
            labelStyle: {
                color: 'silver'
            }
        },

        navigator: {
            handles: {
                backgroundColor: '#666',
                borderColor: '#AAA'
            },
            outlineColor: '#CCC',
            maskFill: 'rgba(255,255,255,0.1)',
            series: {
                color: '#7798BF',
                lineColor: '#A6C7ED'
            },
            xAxis: {
                gridLineColor: '#505053',
            }
        },

        scrollbar: {
            barBackgroundColor: '#808083',
            barBorderColor: '#808083',
            buttonArrowColor: '#CCC',
            buttonBackgroundColor: '#606063',
            buttonBorderColor: '#606063',
            rifleColor: '#FFF',
            trackBackgroundColor: '#404043',
            trackBorderColor: '#404043'
        },

        // special colors for some of the
        legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
        background2: '#505053',
        dataLabelsColor: '#B0B0B3',
        textColor: '#C0C0C0',
        contrastTextColor: '#F0F0F3',
        maskColor: 'rgba(255,255,255,0.3)'
    });

    

    Highcharts.chart('container', {
        "chart": {
            "type": "xrange"
        },
        "title": {
            "text": "State Periods"
        },
        "xAxis": {
            "type": "datetime",
            dateTimeLabelFormats: {
                millisecond: '%H:%M',
                second: '%H:%M:%S',
                minute: '%H:%M',
                hour: '%H:%M',
                day: '%H:%M',
                week: '%H:%M',
                month: '%H:%M',
                year: '%H:%M'
            }
        },
        yAxis: [{
            title: {
                text: "Devices"
            },
            categories: ["VW1", "VW2", "VW3", "VW4", "VW5", "VW6", "VW7", "VW8", "VW9", "VW10"],
            reversed: true
        }],
        "plotOptions": {
            "xrange": {
                "borderRadius": 0,
                "borderWidth": 0,
                "groupPadding": 0.4, // change this if less than 3 devices to 0.45
                "pointPadding": 0.00,
                "grouping": false,
                "dataLabels": {
                    "align": "center",
                    "enabled": true,
                    "format": "{point.name}"
                },
                "colorByPoint": false
            }
        },
        "tooltip": {
            "headerFormat": "<span style=\"font-size: 0.85em\">{point.x} - {point.x2}</span><br/>",
            "pointFormat": "<span style=\"color:{series.color}\">●</span> {series.name}: <b>{point.yCategory}</b><br/>"
        },


        // Temporal data: x = start, x2 = end, y = device
        // current time format is epoch time in milliseconds

        "series": [{
            "name": "Verde",
            "data": [
                {"x": 1707091200000, "x2": 1707174000000, "y": 0},
                {"x": 1707091200000, "x2": 1707174000000, "y": 1},
                {"x": 1707091200000, "x2": 1707174000000, "y": 2},
                {"x": 1707091200000, "x2": 1707174000000, "y": 3},
                {"x": 1707091200000, "x2": 1707174000000, "y": 4},
                {"x": 1707091200000, "x2": 1707174000000, "y": 5},
                {"x": 1707091200000, "x2": 1707174000000, "y": 6},
                {"x": 1707091200000, "x2": 1707174000000, "y": 7},
                {"x": 1707091200000, "x2": 1707174000000, "y": 8},
                {"x": 1707091200000, "x2": 1707174000000, "y": 9}
            ]
        },

        // Temporal data

        {
            "name": "Amarillo",
            "data": [
                {"x": 1707122547795, "x2": 1707126147795, "y": 0},
                {"x": 1707129287468, "x2": 1707143687468, "y": 1},
                {"x": 1707115898784, "x2": 1707123098784, "y": 2},
                {"x": 1707099358385, "x2": 1707113758385, "y": 3},
                {"x": 1707131105871, "x2": 1707134705871, "y": 4},
                {"x": 1707123535865, "x2": 1707137935865, "y": 5},
                {"x": 1707123409860, "x2": 1707127009860, "y": 6},
                {"x": 1707092387128, "x2": 1707099587128, "y": 7},
                {"x": 1707140428427, "x2": 1707147628427, "y": 8},
                {"x": 1707143314191, "x2": 1707146914191, "y": 9}
            ]
        },

        // Temporal data

        {
            "name": "Rojo",
            "data": [
                {"x": 1707096421465, "x2": 1707103621465, "y": 0},
                {"x": 1707120549946, "x2": 1707124149946, "y": 1},
                {"x": 1707154482811, "x2": 1707161682811, "y": 2},
                {"x": 1707096220236, "x2": 1707099820236, "y": 3},
                {"x": 1707095545375, "x2": 1707102745375, "y": 4},
                {"x": 1707106987538, "x2": 1707110587538, "y": 5},
                {"x": 1707143323693, "x2": 1707146923693, "y": 6},
                {"x": 1707118417005, "x2": 1707125617005, "y": 7},
                {"x": 1707105339232, "x2": 1707112539232, "y": 8},
                {"x": 1707108664806, "x2": 1707112264806, "y": 9}
            ]
        }

        ],
    });
});
